<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>ДЗ 1.1. Введение в PHP</title>
	<style type="text/css">
		div {
			display: table;
		}
		ul {
			display: table-row;
		}
		li {
			display: table-cell;
			padding: 0 5px;
		}
	</style>
</head>
<body>
	<?php
	$username = 'Транина Олеся';
	$userage = '45';
	$useramail = 'trole70@mail.ru';
	$location = 'Воронеж';
	$userabout = 'Программист';
	?>
	<ul>
		<li>Имя</li>
		<li><?= $username ?></li>
	</ul>
	<ul>
		<li>Возраст</li>
		<li><?= $userage ?></li>
	</ul>
	<ul>
		<li>Адрес электронной почты</li>
		<li><a href="mailto:<?= $useramail ?>"><?= $useramail ?></a></li>
	</ul>
	<ul>
		<li>Город</li>
		<li><?= $location ?></li>
	</ul>
	<ul>
		<li>О себе</li>
		<li><?= $userabout ?></li>
	</ul>
</body>
</html>